<?php

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;

use function Pest\Faker\faker;
use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(RefreshDatabase::class);

test('list all posts', function () {
  $post = Post::factory()->count(10)->create();

  getJson(route('posts.index'))
    ->assertSuccess()
    ->assertSee($post->title);
});

test('user can create post', function () {
  userSignIn();

  $post = Post::factory()->create();

  postJson(route('posts.store'), [$post])
    ->assertSuccess()
    ->assertRedirect(route('dashboard'))
    ->assertSee($post->title);

  assertDatabaseHas('posts', [
    'title' => $post->title
  ]);
});

test('user can update post', function () {
  userSignIn();

  $post = Post::factory()->create();

  putJson(route('posts.update', $post->id), [
    'title' => 'test1',
    'content' => faker()->sentence(),
  ])
    ->assertSuccess()
    ->assertRedirect(route('dashboard'))
    ->assertSee($post->title);

  assertDatabaseHas('posts', [
    'title' => 'test1'
  ]);
});
